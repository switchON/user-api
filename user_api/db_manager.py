# -*- coding: utf-8 -*-

import bcrypt
import logging

class DBManager:

    def __init__(
        self,
        db_driver,
        host,
        user,
        passwd,
        database
    ):
        self.__db_driver = db_driver
        self._host = host
        self._user = user
        self._passwd = passwd
        self._database = database
        self._connect()

    def _connect(self):
        self._db = self.__db_driver.connect(
            host=self._host,
            user=self._user,
            passwd=self._passwd,
            db=self._database,
            charset=u"utf8"
        )

    def _do_reconnect_if_needed(self, e):
        if e[0] == 2006:
            logging.info(u"Connection lost. Reconnecting ... {}".format(e))
            self._connect()
            logging.info(u"Connection recovered")
        else:
            logging.warning(u"Incident : {}".format(e))
            raise e

    def _execute(self, query, values=None, do_commit=True):
        """
        :type query: string
        :param query: The SQL query to execute
        :type values: List
        :param values: The values to sanitize & pass to the query to replace the "%s" values.
        :type do_commit: Bool
        :param do_commit: If the value has to be saved immediately, or can allow a rollback.
        :return:
        """

        cursor = self._db.cursor()

        try:
            cursor.execute(query, values)
        except self.__db_driver.OperationalError as e:
            self._do_reconnect_if_needed(e)
            cursor.execute(query, values)

        if do_commit:
            cursor.connection.commit()
        return cursor.fetchall(), cursor.description

    def get_user_informations(self, email):
        rows, _ = self._execute(u"SELECT id, email, name FROM user WHERE email = %s", (email,))

        if len(rows) == 0:
            return None

        return {
            u"id": rows[0][0],
            u"email": rows[0][1],
            u"name": rows[0][2]
        }

    def modify_password(self, email, password):
        self._execute(
            u"UPDATE user SET hash = %s WHERE email = %s",
            (bcrypt.hashpw(password.encode(u"utf-8"), bcrypt.gensalt(14)), email)
        )

    def save_new_user(self, email, name, password):
        try:
            self._execute(
                u"INSERT INTO user(email, name, hash) VALUES (%s, %s, %s);",
                (email, name, bcrypt.hashpw(password.encode(u"utf-8"), bcrypt.gensalt(14)))
            )
        except self.__db_driver.IntegrityError as e:
            raise ValueError(str(e))
    def is_user_password_valid(self, email, password):
        rows, _ = self._execute(u"SELECT hash FROM user WHERE email = %s", (email,))
        if len(rows) == 0:
            return False
        return bcrypt.hashpw(password.encode(u"utf-8"), rows[0][0].encode(u"utf-8")) == rows[0][0]