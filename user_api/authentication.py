# -*- coding: utf-8 -*-
import jwt
import datetime
import time


class Authentication:

    def __init__(self, db_manager, secret):
        self._db_manager = db_manager
        self._secret = secret

    def authentify(self, email, password):
        """
        Check if the email / password combination is valid, and sends back
        a valid token.
        Raise a ValueError exception if combination is not valid.
        :param email: The email to check.
        :param password: The password to check.
        :return: A valid JWT token.
        """
        valid = self._db_manager.is_user_password_valid(email, password)
        if valid:
            return self._generate_token(email)
        else:
            raise ValueError(u"Wrong password")

    def _generate_token(self, email):
        """
        Generate a token from the user information.
        :param email: the user email.
        :return: A valid JWT token.
        """
        # Get user informations metadata
        payload = self._db_manager.get_user_informations(email)
        # Update with expiration date
        timestamp = int(time.mktime(datetime.datetime.utcnow().timetuple()))
        payload[u"exp"] = timestamp + (3600 * 24)
        # Create JWT token
        encoded = jwt.encode(
            payload,
            self._secret,
            algorithm=u"HS256"
        )
        # Return
        return encoded

    def is_token_valid(self, token):
        """
        Check the validity of a token, and send back a boolean if
        valid or not.
        :param token: The token to check.
        :return: Boolean value if the token is valid or not.
        """
        decoded = self.get_token_data(token)
        return decoded is not None

    def get_token_data(self, token):
        """

        :param token:
        :return:
        """
        try:
            decoded = jwt.decode(token, self._secret)
        except jwt.ExpiredSignature:
            return None
        except jwt.DecodeError:
            return None
        return decoded

