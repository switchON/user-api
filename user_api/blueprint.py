# -*- coding: utf-8 -*-
from user_api.db_manager import DBManager
from user_api.authentication import Authentication
from flask import request, jsonify, Blueprint
import json


def construct_user_api_blueprint(
        db_driver,
        db_host,
        db_user,
        db_passwd,
        db_name,
        jwt_secret
):
    """
    Take the various parameters necessary to the blue print to work, then
    construct the blueprint from them, to inject in flask configuration.
    :param db_driver:
    :param db_host:
    :param db_user:
    :param db_passwd:
    :param db_name:
    :param jwt_secret:
    :return: Return a blueprint.
    """

    # Init DB Manager
    db_manager = DBManager(
        db_driver,
        host=db_host,
        user=db_user,
        passwd=db_passwd,
        database=db_name
    )

    # Init Auth Manager
    auth = Authentication(
        db_manager,
        secret=jwt_secret
    )

    user_api_blueprint = Blueprint(u'user_api', __name__)

    @user_api_blueprint.route(u'/user/authentify', methods=[u"POST"])
    def user():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422

        if u"password" in data and u"email" in data:
            try:
                token = auth.authentify(
                    email=data.get(u"email"),
                    password=data.get(u"password")
                )
                return jsonify({
                    u"token": token
                }), 200
            except ValueError:
                return jsonify({
                    u"message": u"Wrong login or / and password."
                }), 401
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }), 422

    @user_api_blueprint.route(u'/user/reset_password', methods=['POST'])
    def reset_password():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422
        if u"password" in data and u"email" in data:
            try:

                db_manager.modify_password(
                    data.get(u"email"),
                    data.get(u"password")
                )

                token = auth.authentify(
                    email=data.get(u"email"),
                    password=data.get(u"password")
                )

                return jsonify({
                    u"token": token
                }), 200
            except ValueError:
                return jsonify({
                    u"message": u"Wrong login or / and password."
                }), 401
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }), 422

    @user_api_blueprint.route(u'/user/register', methods=[u"POST"])
    def register():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422

        if u"password" in data and u"email" in data and u"name" in data:
            try:
                db_manager.save_new_user(
                    email=data.get(u"email"),
                    name=data.get(u"name"),
                    password=data.get(u"password")
                )
            except ValueError:
                return jsonify({
                    u"message": u"User already exists."
                }), 409

            return jsonify({
                u"message": u"New user registered successfully."
            }), 201
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }), 422

    @user_api_blueprint.route(u'/user/token/check/', methods=[u"POST"])
    def check():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422
        if u"token" in data:
            decoded = auth.get_token_data(data[u"token"])
            if decoded is not None:
                return jsonify(decoded), 200
            else:
                return jsonify({
                    u"message": u"Invalid token."
                }), 401
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }), 422

    return user_api_blueprint
